//Obtendo a conexão com o banco de dados
const connect = require("../db/connect");

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      bairro,
      cidade,
      estado,
      cep,
      complemento,
      referencia,
    } = req.body;

    //Verificando se o atributo chave é diferente de 0
    if (telefone !== 0) {
      const query = `insert into cliente (telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) 
            values (
            '${telefone}',
            '${nome}',
            '${cpf}',
            '${logradouro}',
            '${numero}',
            '${complemento}',
            '${bairro}',
            '${cidade}',
            '${estado}',
            '${cep}',
            '${referencia}'
            )`; //Fim da query

      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Cliente não cadastrado no banco!" });
            return;
          } //Fim do if
          console.log("Inserido no banco!");
          res.status(201).json({ message: "Usuário criado com sucesso!" });
        });
      } catch (error) {
        console.log("Erro ao executar o insert! - ", error);
        res.status(500).json({ error: "Erro interno do servidor!" });
      } //Fim do catch
    } //Fim do if
    else {
      res.status(400).json({ message: "O telefone é obrigatório!" });
    } //Fim do else
  } //Fim do createCliente

  //Select da tabela cliente
  static async getAllClientes(req, res) {
    const query = `select * from cliente`;

    try {
      connect.query(query, function (err, data) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ message: "Usuários não encontrados no banco!" });
          return;
        } //Fim do if
        let clientes = data;

        console.log("Consulta realizada com sucesso!");
        res
          .status(201)
          .json({ message: "Conteúdo da variável clientes: ", clientes });
      }); //Fim do connect
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!" });
    } //Fim do catch
  } //Fim do getAllClientes

  static async getClientesWhere(req, res) {
    //Método para selecionar clientes via parâmetros específicos

    //Extrair parâmetros da consulta da URL
    const { filtro, ordenacao, ordem } = req.query;

    //Construir a consulta SQL base
    let query = `select * from cliente`;

    //Adicionar a cláusula where, quando houver
    if (filtro) {
      //query = query + filtro; está incorreto
      //query = query + ` where ${filtro}`; está correto mas é mais longa

      query += ` where ${filtro}`;
    }//Fim do if(filtro)

    //Adicionar cláusula order by, quando houver
    if(ordenacao){
      query += ` order by ${ordenacao}`;

      //Adicionar a ordem do order by (asc ou desc)
      if(ordem){
        query += ` ${ordem}`;
      }//Fim do if(ordem)

    }//Fim do if(ordenacao)

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ message: "Usuários não encontrados no banco!" });
          return;
        } //Fim do if

        console.log("Consulta realizada com sucesso!");
        res
          .status(201)
          .json({ message: "Conteúdo: ", result });
      }); //Fim do connect
    } catch (error) {
      console.error("Erro ao executar a consulta: ", error);
      res.status(500).json({ error: "Erro interno do servidor!" });
    } //Fim do catch
  } // Fim do GetClientesWhere
}; //Fim do Module
