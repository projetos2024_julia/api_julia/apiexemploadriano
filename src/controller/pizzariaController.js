const connect = require("../db/connect");

module.exports = class pizzariaController {
  static async listarPedidosPizza(req, res) {
    const query = `
        SELECT
        pp.fk_id_pedido AS Pedido, 
        p.nome AS Pizza,
        pp.quantidade AS Qtde, 
        ROUND((pp.valor / pp.quantidade), 2) AS Unitário,
        pp.valor AS Total
      FROM
        pizza_pedido pp,
        pizza p
      WHERE
        pp.fk_id_pizza = p.id_pizza
      ORDER BY
        pp.fk_id_pedido;
        `; //Fim da atribuição da constante

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          return res
            .status(500)
            .json({ error: "Erro ao consultar o banco de dados" });
        } //Fim do if

        console.log("Consulta realizada com sucesso!!!");
        return res.status(200).json({ result });
      }); //Fim do connect.query
    } catch (error) {
      console.error("Erro ao executar a consulta da pedidos de pizzas:", error);
      return res.status(500).json({ error: "Erro interno do servidor!!!" });
    } //Fim do try catch
  } //Fim do listarPedidosPizza



  static async listarPedidosPizzaComJoin(req, res) {
    const query = `
  select
	  pp.fk_id_pedido as Pedido, p.nome as Pizza,
	  pp.quantidade as Qtde, 
	  round((pp.valor/pp.quantidade),2) as Unitário,
	  pp.valor as Total
  from
	  pizza_pedido pp INNER JOIN pizza p
  on
	  pp.fk_id_pizza = p.id_pizza
  order by pp.fk_id_pedido;
      `; //Fim da atribuição da constante

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          return res
            .status(500)
            .json({ error: "Erro ao consultar o banco de dados" });
        } //Fim do if

        console.log("Consulta com o Join realizada com sucesso!!!");
        return res.status(200).json({ result });
      }); //Fim do connect.query
    } catch (error) {
      console.error("Erro ao executar a consulta da pedidos de pizzas:", error);
      return res.status(500).json({ error: "Erro interno do servidor!!!" });
    } //Fim do try catch
  } //Fim do listarPedidosPizza
}; //Fim do module exports
