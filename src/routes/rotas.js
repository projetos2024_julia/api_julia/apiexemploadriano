const router = require("express").Router();
const dbController = require("../controller/dbController");
const clienteController = require("../controller/clienteController");
const pizzariaController = require("../controller/pizzariaController");

//rota para cunsulta das tabelas do banco
router.get("/tables", dbController.getTables);

//rota para cadastro de clientes
router.post("/postCliente", clienteController.createCliente);

//rota para listar (select) clientes
router.get("/getClientes", clienteController.getAllClientes);

//rota para listar (select) clientes where
router.get("/getClientesWhere", clienteController.getClientesWhere);

//rota para listar Pedidos Pizza
router.get("/listarPedidosPizza", pizzariaController.listarPedidosPizza);

//rota para listar Pedidos Pizza Join
router.get("/listarPedidosJoin", pizzariaController.listarPedidosPizzaComJoin);

module.exports = router;
